# Applied Machine Learning and Time-Series Analysis

## Requirements

We will be using a Jupyter Notebook and a few Python packages.  Follow the steps outlined below to install the recommended Python distribution and the TensorFlow module.

### Python Distribution
he easiest way to setup your environment is to use [Anaconda](https://www.anaconda.com).  There are two ways to get the default Anaconda Python distribution environment.

#### Option A: Anaconda

Download and install [Anaconda](https://www.anaconda.com/distribution/#download-section) using the default settings/prompts.

#### Option B: Miniconda

Download and install [Miniconda](https://docs.conda.io/en/latest/miniconda.html) using the default settings/prompts.  Open the Anaconda Prompt program (or a terminal/shell if using Mac or Linux) run the following commands:
```bash
$ conda create -n anaconda
$ conda activate anaconda
$ conda install anaconda
```

### TensorFlow
The default Anaconda Python distribution environment can be used to install TensorFlow using the `pip` command.  Run the following command from the Anaconda Prompt or a terminal/shell:

```bash
$ pip install tensorflow
```
## Data Set

We will be using the data set below.  A local copy is included (see `energydata_complete.csv`).

> Luis M. Candanedo, Veronique Feldheim, Dominique Deramaix, Data driven prediction models of energy use of appliances in a low-energy house, Energy and Buildings, Volume 140, 1 April 2017, Pages 81-97, ISSN 0378-7788, [Web Link](http://dx.doi.org/10.1016/j.enbuild.2017.01.083).
